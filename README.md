# Sourdough

## Overview
I was given the opportunity in my 2020 summer placement to explore React.js in a professional setting.
This helped me to find the love for innovative web technologies that I once had earlier in my academic career.

I wanted to create an application using some of the up to date tech available now and have wanted a tool
to help me track and improve my baking skills for a while.

Meet **Sourdough**, a simple server-side rendered application using Next.js (A framework that is based on
React.js), that allows you to:
* Keep track of your bakes
* Store and update bread recipes
* Upload images of your bread
* Meet other bakers like you ( well maybe one day :D )

## Contents
1. [Project Structure](project-structure "project structure") 
2. [About the App](about-the-app "about the app") 
3. [Running the App](running-the-app "running the app") 

## Project Structure
```
.
+-- components                                         # React Components
+-- pages                                              # Application Page Entrypoints
|   +-- api                                            # Endpoints for Data Loading
+-- public                                             # Static Assets
+-- styles                                             # Stylesheets
```

## About the App
*Coming Soon*

## Running the App

1. Clone this repo and enter the directory
2. Run `npm install`
3. Run `npm run dev` for a development server which will run on `http://localhost:3000`
3. Run `npm run build` and `npm run start` for a production version of the application
