import Head from "next/head";
import Navigation from "../components/Navigation";

const Layout = ({children, title="Sourdough | A Jonnobrow App"}) => {

    return(
        <div className="pure-g">
          <Head>
            <title>{title}</title>
            <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.3/build/pure-min.css" integrity="sha384-cg6SkqEOCV1NbJoCu11+bm0NvBRc8IYLRGXkmNrqUBfTjmMYwNKPWBTIKyw9mHNJ" crossorigin="anonymous"/>
            <meta name="viewport" content="width=device-width, initial-scale=1" />
          </Head>
          <div className="pure-u-1-5">
            <Navigation />
          </div>
          <div className="pure-u-4-5">
            {children}
          </div>
        </div>
    );

};

export default Layout;
