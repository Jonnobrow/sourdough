import Link from "next/link";

const Navigation = () => 
    <div className="pure-menu">
      <span className="pure-menu-heading">
        Sourdough
      </span>
      <ul className="pure-menu-list">
        <li className="pure-menu-item">
          <Link href="/about">
            <a className="pure-menu-link">About</a>
          </Link>
        </li>
        <li className="pure-menu-item">
          <Link href="/join">
            <a className="pure-menu-link">Join</a>
          </Link>
        </li>
      </ul>
    </div>;

export default Navigation;
