# Sourdough

## Contents
1. Project Overview
2. [Requirements](#requirements)
3. Design
4. Testing

## 1 Project Overview <span id="overview" name="overview"></span>

**Sourdough** is a summer projected relating to a hobby of mine, Sourdough Bread.
I enjoy cooking and eating and bread has always been a highlight for me so in my opinion sourdough bread is the ultimate respect I can pay to the art of bread.

I wanted to create a simple web application that I can host on Gitlab Pages to allow me to easily keep track of my sourdough baking adventure.

Some key criteria:
- Keeping track of starter
- Logging a feed of the started
- Logging a batch of bread
- Uploading images of the bake

## 2 Requirements <span id="requirements" name="requirements"></span>

TODO

## 3 Design <span id="design" name="design"></span>

TODO

## 4 Testing <span id="testing" name="testing"></span>

TODO
