import Head from 'next/head';
import Layout from "../components/Layout";

export default function Home() {
  return (
      <Layout>
          <h1>
          Welcome to Sourdough
          </h1>
          <h3>A Jonnobrow Application</h3>
      </Layout>
  );
}
